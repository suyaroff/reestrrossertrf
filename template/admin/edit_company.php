﻿<section class="std-section">
    <h1>Редактирование компании <? echo htmlspecialchars($comp['short_name']); ?></h1>
    <p><a href="/pdf/cert.php?company=<? echo $comp['id']; ?>&cert=2">Скачать Свидетельство ИК №1</a></p>
    <p><a href="/pdf/cert.php?company=<? echo $comp['id']; ?>&cert=3">Скачать Свидетельство ИК №2</a></p>
    <p><a href="/pdf/cert.php?company=<? echo $comp['id']; ?>&cert=1">Скачать сертик</a></p>
    <? if ($message) { ?>
        <h2><? echo $message; ?></h2>
    <? } ?>
    <form action="" method="post" class="company-form">

        <p>
            <label>ИНН</label>
            <input type="text" class="input-control" name="inn" value="<? echo $comp['inn']; ?>">
        </p>
        <p>
            <label>ОГРН</label>
            <input type="text" class="input-control" name="ogrn" value="<? echo $comp['ogrn']; ?>">
        </p>
        <p>
            <label>Полное наименование</label>
            <input type="text" class="input-control" name="full_name" value="<? echo htmlspecialchars($comp['full_name']); ?>">
        </p>
        <p>
            <label>Сокращенное наименование</label>
            <input type="text" class="input-control" name="short_name" value="<? echo htmlspecialchars($comp['short_name']); ?>">
        </p>
        <p>
            <label>Юридический адрес</label>
            <input type="text" class="input-control" name="adress_doc" value="<? echo htmlspecialchars($comp['adress_doc']); ?>">
        </p>
        <p>
            <label for="">Адрес осуществления деятельности, в соответствии с областью сертификации</label>
            <input type="text" class="input-control" name="adress_fact" value="<? echo htmlspecialchars($comp['adress_fact']); ?>">
        </p>
        <p>
            <label>Должность</label>
            <input type="text" class="input-control" name="work" value="<? echo $comp['work']; ?>">
        </p>
        <p>
            <label>Руководитель</label>
            <input type="text" class="input-control" name="worker" value="<? echo $comp['worker']; ?>">
        </p>
        <p>
            <label>Регистрационный номер</label>
            <input type="text" class="input-control" name="register_number" value="<? echo $comp['register_number']; ?>">
        </p>
        <p>
            <label for="">Порядковый номер</label>
            <input type="text" class="input-control" name="id_main" value="<? echo $comp['id_main']; ?>">
        </p>
        <p>
            <label for="">Дата выдачи</label>
            <input type="text" class="input-control" name="date_open" value="<? echo $comp['date_open']; ?>">
        </p>
        <p>
            <label for="">Действителен до</label>
            <input type="text" class="input-control" name="date_close" value="<? echo $comp['date_close']; ?>">
        </p>
        <p>
            <label for="">Область сертификации</label>
            <select name="cert_oblast" class="input-control">
                <? foreach ($cert_oblast as $o) { ?>
                    <option value="<? echo $o['name']; ?>" <? if ($o['name'] == $comp['cert_oblast']) echo 'selected'; ?> ><? echo $o['name']; ?></option>
                <? } ?>
            </select>
        </p>
        <p>
            <label for="">Предмет сертификации</label>
            <select name="cert_predmet" class="input-control">
                <? foreach ($cert_predmet as $p) { ?>
                    <option value="<? echo $p['name']; ?>" <? if ($p['name'] == $comp['cert_predmet']) echo 'selected'; ?> ><? echo $p['name']; ?></option>
                <? } ?>
            </select>
        </p>

        <p>
            <label for="">1-й инспекционный контроль</label>
            <input type="text" class="input-control" name="date_control_1" value="<? echo $comp['date_control_1']; ?>">
        </p>
        <p>
            <label for="">Статус 1-го инспекционного контроля</label>
            <select name="status_date_1" class="input-control">
                <option value="не пройден" <? if ($comp['status_date_1'] == 'не пройден') echo 'selected'; ?> >не пройден</option>
                <option value="пройден" <? if ($comp['status_date_1'] == 'пройден') echo 'selected'; ?> >пройден</option>
                <option value="просрочен" <? if ($comp['status_date_1'] == 'просрочен') echo 'selected'; ?> >просрочен</option>
            </select>
        </p>
        <p>
            <label for="">2-й инспекционный контроль</label>
            <input type="text" class="input-control" name="date_control_2" value="<? echo $comp['date_control_2']; ?>">
        </p>
        <p>
            <label for="">Статус 2-го инспекционного контроля</label>
            <select name="status_date_2" class="input-control">
                <option value="не пройден" <? if ($comp['status_date_2'] == 'не пройден') echo 'selected'; ?> >не пройден</option>
                <option value="пройден" <? if ($comp['status_date_2'] == 'пройден') echo 'selected'; ?> >пройден</option>
                <option value="просрочен" <? if ($comp['status_date_2'] == 'просрочен') echo 'selected'; ?> >просрочен</option>
            </select>
        </p>
        <p>
            <label for="">1-й аудитор</label>
            <input type="text" class="input-control" name="auditor_1" value="<? echo $comp['auditor_1']; ?>">
        </p>
        <p>
            <label for="">2-й аудитор</label>
            <input type="text" class="input-control" name="auditor_2" value="<? echo $comp['auditor_2']; ?>">
        </p>
        <p>
            <label for="">3-й аудитор</label>
            <input type="text" class="input-control" name="auditor_3" value="<? echo $comp['auditor_3']; ?>">
        </p>
        <p>
            <label for="">Статус сертификата</label>
            <select name="status" class="input-control">
                <option value="0" <? if ($comp['status'] == 0) echo 'selected'; ?> >Приостановлен</option>
                <option value="1" <? if ($comp['status'] == 1) echo 'selected'; ?> >Действителен</option>
            </select>
        </p>


        <p>
            <label for="">КПП</label>
            <input type="text" class="input-control" name="kpp" value="<? echo $comp['kpp']; ?>">
        </p>

        <p>
            <label for="">ОКПО</label>
            <input type="text" class="input-control" name="okpo" value="<? echo $comp['okpo']; ?>">
        </p>
        <p>
            <label for="">Email</label>
            <input type="text" class="input-control" name="email" value="<? echo $comp['email']; ?>">
        </p>
        <p>
            <label for="">Сумма договора</label>
            <input type="amount" class="input-control" name="amount" value="<? echo $comp['amount']; ?>">
        </p>
        <p><label><input type="checkbox" name="refused_ik" value="1" <? if($comp['refused_ik'] == 1)  echo 'checked';?> > Отказался от ИК</label></p>
        <p></p>
        <p>
            <button type="submit">Сохранить</button>
        </p>
        <p>
            <a href="/admin/?action=delete_company&id=<? echo $comp['id']; ?>">Удалить</a>
        </p>

    </form>
</section>