<?php /* Список функций и только */

/**
 * Получение всех данных в табице
 * @param $table
 * @return array
 */
function db_get_all($table)
{
  global $dbh;
  $list = array();
  $sql = "SELECT * FROM  $table";
  $result = $dbh->query($sql, PDO::FETCH_ASSOC);
  foreach ($result as $r) {
    if (isset($r['id'])) {
      $list[$r['id']] = $r;
    } else {
      $list[] = $r;
    }
  }
  return $list;
}

/**
 * Получение данных в таблице с условием
 * @param $table
 * @param $where
 * @return array
 */
function db_get_where($table, $where)
{
  global $dbh;
  $sql = "SELECT * FROM  $table WHERE $where";
  $result = $dbh->query($sql, PDO::FETCH_ASSOC);
  return $result->fetchAll(PDO::FETCH_ASSOC);
}

/**
 * Поиск персонала по полю fio и obrazovan
 * @param $s
 * @return array
 */
function search($s)
{
  global $dbh;
  $s = htmlentities(strip_tags(trim($s)));
  $sql = "SELECT * 
          FROM  company 
          WHERE inn LIKE'%$s%' ";

  $result = $dbh->query($sql, PDO::FETCH_ASSOC);
  $data = array();
  foreach ($result as $r) {
    $data[] = $r;
  }

  return $data;
}

/**
 * Авторизация пользователя если успешно заносим в сессию
 * @param $data
 * @return bool
 */
function login_user($data)
{
  global $dbh;
  $sql = "SELECT * FROM users WHERE email = :email";
  $sth = $dbh->prepare($sql);
  $sth->execute(array(':email' => $data['email']));
  $user = $sth->fetch(PDO::FETCH_ASSOC);
  if (!$user) return false;
  // если пароли верны
  if (password_verify($data['password'], $user['password'])) {
    // создаем сесию пользователя
    $_SESSION['user'] = $user;
    return true;
  } else {
    return false;
  }

}

/**
 * Выход
 */
function logout_user()
{
  unset($_SESSION['user']);
}

/**
 * Хеширование паролей, что бы не хранить в базе в чистом виде
 * @param $password
 * @return bool|string
 */
function my_hash_password($password)
{
  return password_hash($password, PASSWORD_BCRYPT);
}


function redirect($url = '/')
{
  header('Location:' . $url);
  die();
}

function db_insert($table, $data)
{
  global $dbh;
  $col = array_keys($data);
  $col_st = implode(', ', $col);
  $val = array();
  foreach ($data as $k => $v) {
    $val[':' . $k] = $v;
  }
  $val_st = implode(', ', array_keys($val));
  $sql = "INSERT INTO $table ($col_st) VALUES ($val_st)";
  $sth = $dbh->prepare($sql);
  $sth->execute($val);
  return $dbh->lastInsertId();
}


function db_update($table, $data, $where)
{
  global $dbh;

  $val = $col = array();

  foreach ($data as $k => $v) {
    $val[':' . $k] = $v;
    $col[] = $k . ' = :' . $k;
  }

  $col_st = implode(', ', $col);
  $sql = "UPDATE $table SET $col_st WHERE $where";
  $sth = $dbh->prepare($sql);
  $count = $sth->execute($val);
  return $count;
}


function db_delete($table, $where)
{
  global $dbh;

  $sql = "DELETE FROM  $table WHERE $where";
  $count = $dbh->exec($sql);
  return $count;
}

function my_date($unixtime = false)
{
  global $month;

  if ($unixtime) {
    $m = $month[date('n', $unixtime)];
    return date("d $m Yг.", $unixtime);
  } else {
    $m = $month[date('n')];
    return date("d $m Yг.");
  }
}

function num2str($num)
{
  $nul = 'ноль';
  $ten = array(
    array('', 'один', 'два', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'),
    array('', 'одна', 'две', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'),
  );
  $a20 = array('десять', 'одиннадцать', 'двенадцать', 'тринадцать', 'четырнадцать', 'пятнадцать', 'шестнадцать', 'семнадцать', 'восемнадцать', 'девятнадцать');
  $tens = array(2 => 'двадцать', 'тридцать', 'сорок', 'пятьдесят', 'шестьдесят', 'семьдесят', 'восемьдесят', 'девяносто');
  $hundred = array('', 'сто', 'двести', 'триста', 'четыреста', 'пятьсот', 'шестьсот', 'семьсот', 'восемьсот', 'девятьсот');
  $unit = array( // Units
    array('копейка', 'копейки', 'копеек', 1),
    array('рубль', 'рубля', 'рублей', 0),
    array('тысяча', 'тысячи', 'тысяч', 1),
    array('миллион', 'миллиона', 'миллионов', 0),
    array('миллиард', 'милиарда', 'миллиардов', 0),
  );
  //
  list($rub, $kop) = explode('.', sprintf("%015.2f", floatval($num)));
  $out = array();
  if (intval($rub) > 0) {
    foreach (str_split($rub, 3) as $uk => $v) { // by 3 symbols
      if (!intval($v)) continue;
      $uk = sizeof($unit) - $uk - 1; // unit key
      $gender = $unit[$uk][3];
      list($i1, $i2, $i3) = array_map('intval', str_split($v, 1));
      // mega-logic
      $out[] = $hundred[$i1]; # 1xx-9xx
      if ($i2 > 1) $out[] = $tens[$i2] . ' ' . $ten[$gender][$i3]; # 20-99
      else $out[] = $i2 > 0 ? $a20[$i3] : $ten[$gender][$i3]; # 10-19 | 1-9
      // units without rub & kop
      if ($uk > 1) $out[] = morph($v, $unit[$uk][0], $unit[$uk][1], $unit[$uk][2]);
    } //foreach
  } else $out[] = $nul;
  $out[] = morph(intval($rub), $unit[1][0], $unit[1][1], $unit[1][2]); // rub
  $out[] = $kop . ' ' . morph($kop, $unit[0][0], $unit[0][1], $unit[0][2]); // kop
  return trim(preg_replace('/ {2,}/', ' ', join(' ', $out)));
}

/**
 * Склоняем словоформу
 * @ author runcore
 */
function morph($n, $f1, $f2, $f5)
{
  $n = abs(intval($n)) % 100;
  if ($n > 10 && $n < 20) return $f5;
  $n = $n % 10;
  if ($n > 1 && $n < 5) return $f2;
  if ($n == 1) return $f1;
  return $f5;
}

function send_me($to, $subject, $message)
{

  $headers[] = "Content-Type: text/html; charset=UTF-8";
  $headers[] = 'From:  "' . FROM_NAME . '" <' . FROM_EMAIL. '>';
  $headers = implode("\r\n", $headers);
  if (mail($to, $subject, $message, $headers)) {
    return true;
  } else {
    return false;
  }

}