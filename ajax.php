<?php
/**
 * Created by PhpStorm.
 * User: suyar
 * Date: 12.06.2018
 * Time: 20:27
 */

// подключем фаил конфигурации он подключит базу и функции
require_once('config.php');

if (isset($_GET['get_for_map'])) {
  header("Access-Control-Allow-Origin: *");
  $company = array();
  $sql = "SELECT id, short_name, adress_doc, work, worker, latitude, longitude  FROM  company WHERE longitude > 1";
  $result = $dbh->query($sql, PDO::FETCH_ASSOC);
  if ($result) {
    $company = $result->fetchAll(PDO::FETCH_ASSOC);
  }
  echo json_encode($company);
}