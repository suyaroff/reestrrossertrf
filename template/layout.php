<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="robots" content="index,follow">
  <link rel="shortcut icon" href="/favicon.png" type="image/png">
  <title><? echo $title;?></title>
  <link rel="stylesheet" href="/assets/css/normalize.css">
  <link rel="stylesheet" href="/assets/css/styles.css?v0.1.2">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700&subset=cyrillic">
  <link rel="stylesheet" href="http://code.cdn.mozilla.net/fonts/fira.css">
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter49357642 = new Ya.Metrika2({
                    id:49357642,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/tag.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks2");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/49357642" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</head>
<body>

<div class="wrapper">
  <header class="header">
    <div class="empty"></div>
    <a href="/" class="logo">
      <img src="/assets/img/logo.png">
      <div class="logo-text">
        <b>РОССЕРТ</b>
        <small>Единый центр сертификации</small>
      </div>
    </a>
    <div class="about">
      <a target="_blank" href="https://goo.gl/EVhCc5">О системе</a>
      <p>Рег.№: РОСС RU.З1573.04ОЖН0</p>
    </div>
    <div class="locations">
      <p><img src="/assets/img/ico8.png"> Санкт-Петербург</p>
      <p class="online"><img src="/assets/img/ico9.png"> Эксперт на <span>связи</span></p>
    </div>
    <div class="phone">
      Бесплатная горячая линия
      <a href="tel:8(800)200-67-91">8(800)200-67-91</a>
    </div>

  </header>
  <? if (isset($_SESSION['user']) && $_SESSION['user']['role'] == 1) include ROOT_PATH.'/template/admin/nav.php';?>

  <main class="main">
    <? include ROOT_PATH.'/template/'.$template;?>
  </main>
<!--<section id="section_8">
    <h2 class="section-title">О системе «Россертификация»</h2>
    <div class="container">
      <div class="col-2">
        <h3>О системе</h3>
        <div class="row">
          <div>
            <p>
              Система сертификации «Россертификация» основана<br>
              <strong>ООО «Единый Центр Сертификации»</strong> и зарегистрирована<br>
              в Федеральном агентстве по техническому регулированию<br>
              и метрологии «Росстандарт» за номером <br>РОСС RU.З1573.04ОЖН0.
            </p>
           
          </div>
          <div>
            <a target="_blank" href="assets/docs/f_acc.jpg"><img class="lic" src="assets/img/lic.jpg" alt=""></a>
          </div>
        </div>
        <br>
      </div>
	  <div class="col-2">
        <h3>Контакты и реквизиты</h3>
        <div class="row">
          <div>
            <p>
              ООО «Единый Центр Сертификации» <br>
              198216, Санкт-Петербург , проспект Народного Ополчения,<br>дом 10, литера А пом. 253Н<br>
              ИНН/КПП/ОГРН: 7805672696 / 780501001 / 1167847220177<br>
              depart@mailrossert.ru / 8-800-200-67-91
            </p>
			
          </div>
        </div>
        <br>
      </div>
    </div>
  </section>-->
  <footer class="footer">

    <!-- <div class="col">
      <img src="/assets/img/ico4.png" alt="1" class="img"/>
      Министерство экономического развития
      Российской Федерации
    </div>
    <div class="col">
      <img src="/assets/img/ico5.png" alt="1" class="img"/>
      Федеральное агентство по техническому<br>
      регулированию и метрологии
    </div>
    <div class="col">
      <img src="/assets/img/ico6.png" alt="1" class="img"/>
      Министерство труда и социальной защиты
      Российской Федерации
    </div> -->
    <div class="col">
      &copy; 2018 г., Официальный Интернет-ресурс СДС «Россертификация». Все права на
      материалы, охраняются в соответствии с законодательством Российской Федерации.
    </div>

  </footer>

</div>

<div id="overlay">
  <div id="overlayBox">
    <div id="overlayPad"></div>
  </div>
</div>

<script src="/assets/js/jquery-3.3.1.min.js" type="text/javascript"></script>
<script src="/assets/js/script.js?v0.2.2" type="text/javascript"></script>
</body>
</html>