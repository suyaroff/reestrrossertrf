<section class="std-section">
  <h1>Добавление компании</h1>
  <? if($message) { ?>
    <h2><? echo $message;?></h2>
  <? } ?>
  <form action="" method="post" class="company-form">
    <p>
      <label>ИНН </label>
      <input type="text" class="input-control" name="inn" autocomplete="off">
    </p>
    <p>
      <label>ОГРН <button type="button" onclick="parceBy();">Найти и заполнить</button></label>
      <input type="text" class="input-control" name="ogrn"  autocomplete="off" >
    </p>
    <p>
      <label>Полное наименование</label>
      <input type="text" class="input-control" name="full_name" >
    </p>
    <p>
      <label>Сокращенное наименование</label>
      <input type="text" class="input-control" name="short_name" >
    </p>
    <p>
      <label>Юридический адрес</label>
      <input type="text" class="input-control" name="adress_doc">
    </p>
    <p>
      <label for="">Адрес осуществления деятельности, в соответствии с областью сертификации</label>
      <input type="text" class="input-control" name="adress_fact" >
    </p>
    <p>
      <label>Должность</label>
      <input type="text" class="input-control" name="work">
    </p>
    <p>
      <label>Руководитель</label>
      <input type="text" class="input-control" name="worker" >
    </p>
    <p>
      <label>Регистрационный номер</label>
      <input type="text" class="input-control" name="register_number" >
    </p>
    <p>
      <label for="">Порядковый номер</label>
      <input type="text" class="input-control" name="id_main" >
    </p>
    <p>
      <label for="">Дата выдачи</label>
      <input type="text" class="input-control" name="date_open" value="<? echo date("Y-m-d");?>">
    </p>
    <p>
      <label for="">Действителен до</label>
      <input type="text" class="input-control" name="date_close" value="<? echo date("Y-m-d", strtotime('+3 year'));?>">
    </p>
    <p>
      <label for="">Область сертификации</label>
      <select name="cert_oblast" class="input-control">
        <? foreach ($cert_oblast as $o) { ?>
          <option value="<? echo $o['name'];?>"><? echo $o['name'];?></option>
        <? } ?>
      </select>
    </p>
    <p>
      <label for="">Предмет сертификации</label>
      <select name="cert_predmet" class="input-control">
        <? foreach ($cert_predmet as $p) { ?>
          <option value="<? echo $p['name'];?>"><? echo $p['name'];?></option>
        <? } ?>
      </select>
    </p>

    <p>
      <label for="">1-й инспекционный контроль</label>
      <input type="text" class="input-control" name="date_control_1" value="<? echo date("Y-m-d", strtotime('+1 year'));?>">
    </p>
    <p>
      <label for="">Статус 1-го инспекционного контроля</label>
      <select name="status_date_1" class="input-control">
        <option value="не пройден" selected>не пройден</option>
        <option value="пройден">пройден</option>
        <option value="просрочен">просрочен</option>
      </select>
    </p>
    <p>
      <label for="">2-й инспекционный контроль</label>
      <input type="text" class="input-control" name="date_control_2" value="<? echo date("Y-m-d", strtotime('+2 year'));?>">
    </p>
    <p>
      <label for="">Статус 2-го инспекционного контроля</label>
      <select name="status_date_2" class="input-control">
        <option value="не пройден" selected>не пройден</option>
        <option value="пройден">пройден</option>
        <option value="просрочен">просрочен</option>
      </select>
    </p>
    <p>
      <label for="">1-й аудитор</label>
      <input type="text" class="input-control" name="auditor_1">
    </p>
    <p>
      <label for="">2-й аудитор</label>
      <input type="text" class="input-control" name="auditor_2">
    </p>
    <p>
      <label for="">3-й аудитор</label>
      <input type="text" class="input-control" name="auditor_3">
    </p>
    <p>
      <label for="">Статус сертификата</label>
      <select name="status" class="input-control">
        <option value="0">Приостановлен</option>
        <option value="1" selected>Действителен</option>
      </select>
    </p>


    <p>
      <label for="">КПП</label>
      <input type="text" class="input-control" name="kpp" >
    </p>

    <p>
      <label for="">ОКПО</label>
      <input type="text" class="input-control" name="okpo" >
    </p>
    <p>
      <label for="">Email</label>
      <input type="text" class="input-control" name="email" >
    </p>
    <p>
      <label for="">Сумма договора</label>
      <input type="amount" class="input-control" name="amount" value="12000">
    </p>

    <p>
      <button type="submit">Добавить</button>
    </p>
    <p>
      <button type="reset">Сбросить форму</button>
    </p>


  </form>
</section>