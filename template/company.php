﻿<section class="card">
  <div class="card-header">
    <h2 class="card-title">
      РЕЕСТР
      <small>СДС «РОССЕРТИФИКАЦИЯ»</small>
    </h2>
  </div>
  <div class="card-body">
    <div class="card-col">
      <div class="title">Результаты поиска</div>
      <div class="grey-line"></div>
      <h3 class="col-header">Реквизиты организации</h3>
      <div class="title-1">Полное наименование</div>
      <p><? echo $company['full_name'];?></p>

      <div class="title-1">Сокращенное наименование</div>
      <p><? echo $company['short_name'];?></p>

      <div class="title-1">ИНН</div>
      <p><? echo $company['inn'];?></p>

      <div class="title-1">ОГРН</div>
      <p><? echo $company['ogrn'];?></p>

      <div class="title-1">Юридический адрес</div>
      <p><? echo $company['adress_doc'];?></p>

      <div class="title-1">Адрес осуществления деятельности,
        в соответствии с областью сертификации
      </div>
      <p><? echo $company['adress_fact'];?></p>

      <div class="title-1">Руководитель</div>
      <p><? echo $company['work'];?>, <? echo $company['worker'];?></p>
    </div>
    <div class="card-col">
      <div class="title">&nbsp;</div>
      <div class="grey-line"></div>
      <h3 class="col-header">Данные сертификата</h3>
      <div class="title-1">Регистрационный номер</div>
      <p><? echo $company['register_number'];?></p>
      <div class="title-1">Порядковый номер</div>
      <p><? echo $company['id_main'];?></p>
      <div class="title-1">Дата выдачи</div>
      <p><? echo my_date(strtotime($company['date_open']));?></p>

      <div class="title-1">Действителен до</div>
      <p><? echo my_date(strtotime($company['date_close']));?></p>

      <div class="title-1">1-й инспекционный контроль</div>
      <p>
        Дата: <? echo my_date(strtotime($company['date_control_1']));?>
        Статус: <b class="<? if($company['status_date_1'] == 'пройден'){ ?>success<? }else{ ?>danger<? } ?>"><? echo $company['status_date_1'];?>.</b>
      </p>

      <div class="title-1">2-й инспекционный контроль</div>
      <p>
        Дата: <? echo my_date(strtotime($company['date_control_2']));?>
        Статус: <b class="<? if($company['status_date_2'] == 'пройден'){ ?>success<? }else{ ?>danger<? } ?>"><? echo $company['status_date_2'];?>.</b>
      </p>

      <div class="title-1">Предмет сертификации</div>
      <p><? echo $company['cert_predmet'];?></p>

      <div class="title-1">Область сертификации</div>
      <p><? echo $company['cert_oblast'];?></p>

      <div class="title-1">Внутренние аудиторы</div>
      <p><? echo $company['auditor_1'];?><br> <? echo $company['auditor_2'];?><br> <? echo $company['auditor_3'];?></p>
    </div>
    <div class="card-col">
      <div class="title">&nbsp;</div>
      <div class="grey-line"></div>
      <h3 class="col-header">Статус сертификата</h3>
      <? if($company['status']){ ?>
      <div class="cert_active">ДЕЙСТВИТЕЛЕН</div>
      <? } else { ?>
      <div class="cert_stopped">ПРИОСТАНОВЛЕН</div>
      <? } ?>

      <!--<div class="grey-line"></div>
      <h3 class="col-header">Скан-копия сертификата</h3>
      <a class="cert-download" href="<? echo $company['cert'];?>">Скачать<br>(<? echo $company['cert_size_kb'];?> Кб, pdf)</a>-->


    </div>
  </div>
</section>