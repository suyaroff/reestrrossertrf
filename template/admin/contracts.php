﻿<section class="std-section">
  <h1>Сделать договор</h1>
  <? if($message) { ?>
    <h2><? echo $message;?></h2>
  <? } ?>
  <form action="" method="post" class="company-form">
    <p>
      <label for="">Номер договора</label>
      <input type="text" class="input-control" name="num" >
    </p>
    <p>
      <label for="">Дата договора</label>
      <input type="amount" class="input-control" name="date_contract" value="<? echo $date_created; ?>" onchange="generateContractNum()">
    </p>
    <p>
      <label>ИНН <button type="button" onclick="parceByInn();">Найти и заполнить</button></label>
      <input type="text" class="input-control" name="inn" onchange="generateContractNum()">
    </p>
    <p>
      <label>ОГРН</label>
      <input type="text" class="input-control" name="ogrn" >
    </p>
    <p>
      <label>Полное наименование</label>
      <input type="text" class="input-control" name="full_name" >
    </p>
    <p>
      <label>Сокращенное наименование</label>
      <input type="text" class="input-control" name="short_name" >
    </p>
    <p>
      <label>Юридический адрес</label>
      <input type="text" class="input-control" name="adress_doc">
    </p>
    <p>
      <label>Должность</label>
      <input type="text" class="input-control" name="work">
    </p>
    <p>
      <label>Руководитель</label>
      <input type="text" class="input-control" name="worker" >
    </p>
    <p>
      <label for="">Предмет сертификации</label>
      <select name="cert_predmet" class="input-control">
        <? foreach ($cert_predmet as $p) { ?>
          <option value="<? echo $p['name'];?>"><? echo $p['name'];?></option>
        <? } ?>
      </select>
    </p>
    <p>
      <label for="">Банк</label>
      <input type="text" class="input-control" name="bank" >
    </p>
    <p>
      <label for="">Расчетный счет</label>
      <input type="text" class="input-control" name="r_schet" >
    </p>
    <p>
      <label for="">Корреспондентский счет</label>
      <input type="text" class="input-control" name="k_schet" >
    </p>
    <p>
      <label for="">Бик</label>
      <input type="text" class="input-control" name="bik" >
    </p>
    <p>
      <label for="">КПП</label>
      <input type="text" class="input-control" name="kpp" >
    </p>

    <p>
      <label for="">ОКПО</label>
      <input type="text" class="input-control" name="okpo" >
    </p>
    <p>
      <label for="">Email</label>
      <input type="text" class="input-control" name="email" >
    </p>
    <p>
      <label for="">Сумма договора</label>
      <input type="amount" class="input-control" name="amount" value="12000">
    </p>

    <p style="padding-bottom: 20px;">
      <label><input type="checkbox" name="credit" value="1">В рассрочку</label>

    </p>
    <p></p>
    <p>
      <button type="submit">Сбацать</button>
    </p>
    <p>
      <button type="reset">Сбросить форму</button>
    </p>


  </form>
</section>