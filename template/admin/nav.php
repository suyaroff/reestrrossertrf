<nav class="admin-nav">
  <a href="/admin">Админ</a>
  <a href="/admin?action=add_company">Добавить компанию</a>
  <a href="/admin?action=control">Инспекционный контроль</a>
  <a href="/admin?action=email">Разослать уведомления</a>
  <a href="/admin?action=coordinates">Координаты для компаний</a>
  <a href="/admin?action=contracts">Договора</a>
  <a href="?logout=1">Выход</a>
</nav>