<?php

// подключем фаил конфигурации он подключит базу и функции
include_once '../config.php';
// подключаем шаблон и показываем страницу
$template = 'company.php';
$title = '';
$company_id = (int)$_GET['id'];
if(!$company_id) redirect();
$company = db_get_where('company', "id = $company_id");
$company = $company[0];
$company['cert'] = '/complete/'.$company['date_open'].'_'.$company['inn'].'.pdf';
$file = CERT_PATH.'/'.$company['date_open'].'_'.$company['inn'].'.pdf';
$company['cert_size'] = @filesize($file);
$company['cert_size_kb'] = round($company['cert_size']/1024, 0);
include_once ROOT_PATH."/template/layout.php";