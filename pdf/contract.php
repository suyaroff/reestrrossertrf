<?php
/**
 * Created by PhpStorm.
 * User: suyar
 * Date: 03.06.2018
 * Time: 0:43
 */

include_once '../config.php';
require ROOT_PATH . '/vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;

$company_id = 0;
if (isset($_GET['company'])) $company_id = (int)$_GET['company'];

if ($company_id) {
    $comp = db_get_where('company', "id = $company_id");
    if (count($comp)) {
        $company = $comp[0];
        $company['now'] = date("d.m.Y");
        $company['summa'] = num2str($company['amount']);
        $company['day'] = date("d");
        $company['month'] = date("m");

        /* номера просроченых контролей */
        $ik = 1;
        if (isset($_GET['ik']) && $_GET['ik'] == 2) $ik = '2';

        $patterns = array_map(function ($k) {
            return "/\{$k\}/";
        }, array_keys($company));


        // Загружаем шаблон
        $html = file_get_contents('http://xn--e1aa5aceg.xn--e1arfcdaj.xn--p1ai/template/pdf/contract/' . $ik . '.html');
        $html = preg_replace($patterns, array_values($company), $html);
        $html = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');
        //echo  $html;
        $options['isJavascriptEnabled'] = false;
        $options['isRemoteEnabled'] = true;
        $options['isHtml5ParserEnabled'] = true;
        $options['fontDir'] = ROOT_PATH . '/pdf/fonts';

        $dompdf = new Dompdf($options);
        $dompdf->loadHtml($html, 'UTF-8');

        $dompdf->setPaper('A4');
        $dompdf->render();
        $file_name = "Dogovor_$company[register_number]_ik_$ik.pdf";
        $dompdf->stream($file_name);
    } else {
        echo "Company not exists";
    }

}

$contract_id = 0;
if (isset($_GET['id'])) $contract_id = (int)$_GET['id'];
if ($contract_id) {
    $contract = db_get_where('contracts', "id = $contract_id");
    if (count($contract)) {

        $contract = $contract[0];

        $contract['now'] = date("d.m.Y");
        $contract['summa'] = num2str($contract['amount']);
        $contract['day'] = date("d");
        $contract['month'] = date("m");
        $contract['date_contract'] = date("d.m.Y", strtotime($contract['date_contract']));

        $patterns = array_map(function ($k) {
            return "/\{$k\}/";
        }, array_keys($contract));
        // Загружаем шаблон


        $template_file = 'http://xn--e1aa5aceg.xn--e1arfcdaj.xn--p1ai/template/pdf/contract/3.html';
        if($contract['credit'] == 1) $template_file = 'http://xn--e1aa5aceg.xn--e1arfcdaj.xn--p1ai/template/pdf/contract/4.html';

        $html = file_get_contents($template_file);
        $html = preg_replace($patterns, array_values($contract), $html);
        $html = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');

        $options['isJavascriptEnabled'] = false;
        $options['isRemoteEnabled'] = true;
        $options['isHtml5ParserEnabled'] = true;
        $options['fontDir'] = ROOT_PATH . '/pdf/fonts';

        $dompdf = new Dompdf($options);
        $dompdf->loadHtml($html, 'UTF-8');

        $dompdf->setPaper('A4');
        $dompdf->render();
        $out = $dompdf->output();
        file_put_contents(ROOT_PATH . '/dogovora/' . $contract['num'] . '.pdf', $out);
        header('Location:http://xn--e1aa5aceg.xn--e1arfcdaj.xn--p1ai/dogovora/' . $contract['num'] . '.pdf');
    }
}