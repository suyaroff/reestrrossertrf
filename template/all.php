<section class="all-section">
  <h1>Все записи</h1>
  <table>
    <thead>
    <tr>
      <th>Краткое наименование</th>
      <th>ИНН</th>
      <th>ОГРН</th>
      <th>Рег. номер сертификата</th>
      <th>Дата выдачи</th>
      <th>Статус сертификата</th>
    </tr>
    </thead>
    <tbody>
    <? foreach ($companies as $c){ ?>
    <tr>
      <td>
        <a href="/company?id=<? echo $c['id'];?>"><? echo $c['short_name'];?></a>
      </td>
      <td><? echo $c['inn'];?></td>
      <td><? echo $c['ogrn'];?></td>
      <td><? echo $c['register_number'];?></td>
      <td><? echo $c['date_open'];?></td>
      <td><? if($c['status']) echo 'Действителен'; else echo 'Приостановлен';?></td>
    </tr>
    <? } ?>
    </tbody>
  </table>
</section>