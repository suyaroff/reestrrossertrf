<section class="all-section">
  <h1>Инспекционный контроль</h1>
  <? if(count($companies)){?>
    <p><a href="/admin/?action=control&update_status=1">Обновить статус</a></p>
  <? }?>
  <table>
    <thead>
    <tr>
      <th>Краткое наименование</th>
      <th>ИНН</th>
      <th>Дата выдачи</th>
      <th>Действителен до</th>
      <th>1-й инспекционный контроль</th>
      <th>2-й инспекционный контроль</th>
      <th>Статус сертификата</th>
    </tr>
    </thead>
    <tbody>
    <? foreach ($companies as $c){ ?>
      <tr>
        <td>
          <a href="/admin/?action=edit_company&id=<? echo $c['id'];?>"><? echo $c['short_name'];?></a>
        </td>
        <td><? echo $c['inn'];?></td>
        <td><? echo $c['date_open'];?></td>
        <td style="<? if($now == $c['date_close']) echo 'color: red';?>"><? echo $c['date_close'];?></td>
        <td style="<? if($now == $c['date_control_1']) echo 'color: red';?>"><? echo $c['date_control_1'];?> / <? echo $c['status_date_1'];?></td>
        <td style="<? if($now == $c['date_control_2']) echo 'color: red';?>"><? echo $c['date_control_2'];?> / <? echo $c['status_date_2'];?></td>
        <td><? if($c['status']) echo 'действителен'; else echo 'приостановлен';?></td>
      </tr>
    <? } ?>
    </tbody>
  </table>
</section>