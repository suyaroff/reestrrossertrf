<?php

/*подключем фаил конфигурации он подключит базу и функции */
require_once('../config.php');
// подключаем шаблон и показываем страницу
$template = 'admin/index.php';
$title = 'Админ';

// авторизация
if (!isset($_SESSION['user'])) {
    $errors = '';
    if (isset($_POST['action']) && $_POST['action'] == 'login') {
        if (login_user($_POST)) {
            redirect('/admin');
        } else {
            $errors = 'Не верный логин или пароль';
        }
    }
    $template = 'admin/login.php';
    include_once ROOT_PATH . "/template/layout.php";
    exit;
}

$action = isset($_GET['action']) ? $_GET['action'] : 'index';

switch ($action) {
    /* добавление компании*/
    case 'add_company':
        $message = '';
        $cert_oblast = db_get_all('cert_oblast');
        $cert_predmet = db_get_all('cert_predmet');
        if (count($_POST) && !empty($_POST['inn'])) {
            $_POST = array_map('trim', $_POST);
            $company_id = db_insert('company', $_POST);
            if ($company_id) {
                // загрузка сертфиката
                $cert = file_get_contents('http://xn--e1aa5aceg.xn--e1arfcdaj.xn--p1ai/pdf/cert.php?company=' . $company_id);
                $file = CERT_PATH . '/' . $_POST['date_open'] . '_' . $_POST['inn'] . '.pdf';
                file_put_contents($file, $cert);
                $message = 'Компания добавлена';
            } else {
                $message = 'Ошибка компания не добавлена';
            }
        }
        $template = 'admin/add_company.php';
        break;
    /* редактирование компании */
    case 'edit_company':
        $company_id = (int)$_GET['id'];
        if (!$company_id) redirect('/admin');
        $comp = db_get_where('company', "id = $company_id")[0];
        $message = '';
        $cert_oblast = db_get_all('cert_oblast');
        $cert_predmet = db_get_all('cert_predmet');

        if (count($_POST) && !empty($_POST['inn'])) {
            $new_data = array_map('trim', $_POST);
            $new_data['refused_ik'] = isset($new_data['refused_ik'])? 1: 0;
            $updated = db_update('company', $new_data, " id = $company_id");
            if ($updated) {
                $message = 'Компания сохранена';
                $comp = db_get_where('company', "id = $company_id")[0];
            } else {
                $message = 'Ошибка компания не сохранена';
            }
        }
        $template = 'admin/edit_company.php';
        break;
    /* удаление компании */
    case 'delete_company':
        $company_id = (int)$_GET['id'];
        $comp = db_get_where('company', 'id = ' . $company_id);
        $comp = $comp[0];
        db_delete('company', "id = $company_id");
        $file = CERT_PATH . '/' . $comp['date_open'] . '_' . $comp['inn'] . '.pdf';
        @unlink($file);
        redirect('/admin/');
        break;


    /* инспекционный контроль */
    case 'control':
        $now = date('Y-m-d');
        /* обновление статусов */
        if (isset($_GET['update_status'])) {
            // у тех укого дата вышла
            $sql_update = "UPDATE company SET status = 0 WHERE date_close <= '$now'";
            $dbh->query($sql_update);
            // первая контрольная дата
            $sql_update = "UPDATE company SET status = 0, status_date_1 = 'просрочен' WHERE date_control_1 <= '$now' AND status_date_1 ='не пройден'";
            $dbh->query($sql_update);
            // вторая контрольная дата
            $sql_update = "UPDATE company SET status = 0, status_date_2 = 'просрочен' WHERE date_control_2 <= '$now' AND status_date_2 ='не пройден'";
            $dbh->query($sql_update);

        }
        $sql = " status_date_1 = 'пройден' OR status_date_2 = 'пройден' ";
        $companies = db_get_where('company', $sql);
        $template = 'admin/control.php';
        break;
    /* рассылка уведомлений*/
    case 'email':
        $now = date("Y-m-d", strtotime('- 1 month'));

        $sql = "SELECT COUNT(id) FROM company 
            WHERE status = 0 AND email <> '' AND ('$now' > date_email OR date_email IS NULL)  ";
        $count_company = $dbh->query($sql)->fetchColumn(0);
        $template = 'admin/email.php';
        break;
    /* координаты для компаний */
    case 'coordinates':
        if (isset($_GET['get'])) {
            $company = db_get_where('company', 'longitude IS NULL ');
            foreach ($company as $c) {
                $geo = json_decode(file_get_contents('https://geocode-maps.yandex.ru/1.x/?format=json&geocode=' . urlencode($c['adress_doc'])));
                if (isset($geo->response) &&
                    isset($geo->response->GeoObjectCollection) &&
                    isset($geo->response->GeoObjectCollection->metaDataProperty) &&
                    $geo->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found > 0
                ) {
                    $coords = explode(" ", $geo->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos);
                    $long = trim($coords[0]);
                    $lat = trim($coords[1]);
                    if ($long && $lat) {
                        db_update('company', array('longitude' => $long, 'latitude' => $lat), ' id = ' . $c['id']);
                    }
                }

            }

        }
        $sql = "SELECT COUNT(id) FROM company WHERE longitude IS NULL ";
        $count_company = $dbh->query($sql)->fetchColumn(0);
        $template = 'admin/coordinates.php';
        break;
    /* договора */
    case 'contracts':
        $message = '';
        $cert_predmet = db_get_all('cert_predmet');
        $date_created = date("Y-m-d");
        if (count($_POST)) {
            if (!empty($_POST['inn'])) {
                $_POST = array_map('trim', $_POST);
                $_POST['date_created'] = $date_created;
                $contract_id = db_insert('contracts', $_POST);
                if ($contract_id) {
                    $message = 'Договор создан';
                    header('Location:http://xn--e1aa5aceg.xn--e1arfcdaj.xn--p1ai/pdf/contract.php?id=' . $contract_id);
                } else {
                    $message = 'Ошибка договор не создан';
                }
            }
        } else {
            $message = 'Введите ИНН';
        }

        $template = 'admin/contracts.php';
        break;

    /* главная админки */
    default:
        if (isset($_GET['delete_company'])) {
            $company_id = (int)$_GET['delete_company'];
            $comp = db_get_where('company', 'id = ' . $company_id);
            $comp = $comp[0];
            db_delete('company', "id = $company_id");
            $file = CERT_PATH . '/' . $comp['date_open'] . '_' . $comp['inn'] . '.pdf';
            @unlink($file);
            redirect('/admin/');
        }
        $companies = db_get_all('company');
        break;

}

include_once ROOT_PATH . "/template/layout.php";




