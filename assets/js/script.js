$('#overlay').on('click', function (e) {
    $(this).hide();
});

function modalOpen(c) {
    $('#overlay').show();
    $('#overlayPad').html(c);
}

function modalClose() {
    $('#overlay').hide();
    $('#overlayPad').html('');
}


var preload = 'Поиск <img src="/assets/img/preloader.gif" alt="Loading" style="vertical-align: middle"/>';

//modalOpen('<div class="nothing">Данные не найдены</div>');

/* поиск */
function searchCompany() {
    event.preventDefault();
    var str = $('#inputSearch').val();
    var type = $('.custom-radio:checked').val();
    var tbl = '<div class="tbl-header">\n' +
        '      <table cellpadding="0" cellspacing="0" border="0">\n' +
        '        <thead>\n' +
        '        <tr>\n' +
        '          <th>Краткое наименование</th>\n' +
        '          <th>ИНН</th>\n' +
        '          <th>ОГРН</th>\n' +
        '          <th>Должность/ФИО</th>\n' +
        '        </tr>\n' +
        '        </thead>\n' +
        '      </table>\n' +
        '    </div>\n' +
        '    <div class="tbl-content">\n' +
        '      <table cellpadding="0" cellspacing="0" border="0">\n' +
        '        <tbody>';
    if (str) {
        $.ajax({
            url: '/',
            method: 'post',
            dataType: 'JSON',
            data: {s: str, t: type},
            beforeSend: function (e) {
                modalOpen(preload);
            },
            success: function (r) {
                if (r.length > 1) {
                    tbl = '<h2 class="found-num">Найдено комапний: ' + r.length + '</h2>' + tbl;
                    $.each(r, function (i, v) {
                        tbl += '<tr><td><a href="/company?id=' + v.id + '">' + v.short_name + '</a></td><td>' + v.inn + '</td><td>' + v.ogrn + '</td><td>' + v.work + ', ' + v.worker + '</td></tr>';
                    });
                    tbl += '</tbody></table></div>';
                    $('#searchResult').html(tbl);
                    modalClose();
                } else if (r.length == 1) {
                    window.location = '/company?id=' + r[0].id;
                } else {
                    $('#overlayPad').html('Ничего не найдено');
                }
            }
        });
    } else {
        modalOpen('Введите инн');
    }

}

function parceBy() {
    var str = $('.input-control[name=ogrn]').val();
    if (str) {
        $.getJSON('/admin/parse.php', {str: str}, function (r) {
            if (r.error) {
                alert(r.error);
            } else {
                $.each(r.data, function (i, v) {
                    $('.input-control[name=' + i + ']').val(v);
                });
            }
        })
    } else {
        alert('Введите ИНН');
    }
}

function generateContractNum() {
    var inn = $('input[name=inn]').val();
    var dateContract = $('input[name=date_contract]').val();
    var contractNum = inn.substr(0, 4) + '-' + dateContract.slice(8, 10) + dateContract.slice(5, 7) + dateContract.slice(0, 4);
    //console.log(contractNum);
    $('input[name=num]').val(contractNum);
}
