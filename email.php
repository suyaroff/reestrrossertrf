<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

include_once 'config.php';
require ROOT_PATH . '/vendor/autoload.php';

$pastMonth = date("Y-m-d", strtotime('- 1 month'));

$is_test = isset($_GET['test']) ? $_GET['test'] : 0;


$sql = "SELECT id, short_name, inn, ogrn, worker, email, cert_predmet, register_number, 
        date_open, date_control_1, date_control_2, status_date_1, status_date_2
        FROM company
       WHERE status = 0 AND email <> '' AND ('$pastMonth' > date_email OR date_email IS NULL) ";

if ($is_test) {
  $sql = $sql .=" AND id = " . $is_test;
}

$sth = $dbh->prepare($sql);
$sth->execute();

$patterns = array(
  '/\{id\}/',
  '/\{short_name\}/',
  '/\{inn\}/',
  '/\{ogrn\}/',
  '/\{worker\}/',
  '/\{email\}/',
  '/\{cert_predmet\}/',
  '/\{register_number\}/',
  '/\{date_open\}/',
  '/\{now\}/',
  '/\{date_control_1\}/',
  '/\{date_control_2\}/',
  '/\{status_date_1\}/',
  '/\{status_date_2\}/',
);

/* шаблон для первого контроля */
$template = file_get_contents(ROOT_PATH . '/template/email/1.html');
/* шаблон для второго контроля */
$template2 = file_get_contents(ROOT_PATH . '/template/email/2.html');


/* подготовка почтовика */
$mail = new PHPMailer();
$mail->SMTPDebug = 0;
$mail->isSMTP();
//$mail->isMail();
$mail->CharSet = 'UTF-8';
$mail->Host = SMTP_HOST;
$mail->SMTPAuth = true;
$mail->Username = SMTP_USER;
$mail->Password = SMTP_PASS;
$mail->SMTPSecure = SMTP_SECR;
$mail->Port = SMTP_PORT;
$mail->setLanguage('ru');
$mail->setFrom(FROM_EMAIL, FROM_NAME);
$mail->isHTML(true);
$i = $i2 =  0;

while ($c = $sth->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
  $is_send = $is_send2 = 0;
  $c['now'] = date("d.m.Y");
  $c['date_open'] = date("Y.m.d", strtotime($c['date_open']));

  $mail->addAddress($c['email'], $c['worker']);
  $mail->addBCC(TECH_EMAIL);

  /* тест вида письма*/
  if ($is_test) {
    echo preg_replace($patterns, array_values($c), $template);
    echo preg_replace($patterns, array_values($c), $template2);
    return;
  }

  /* первый контроль */
  if ($c['status_date_1'] == 'просрочен') {
    // тема письма при первом контроле
    $mail->Subject = 'Уведомление от СДС "РОССЕРТИФИКАЦИЯ" о необходимости проведения первого инспекционного контроля';
    $mail->Body = preg_replace($patterns, array_values($c), $template);
    $is_send = $mail->send();
  }

  /* второй контроль */
  if ($c['status_date_2'] == 'просрочен') {
    // тема письма при втором контроле
    $mail->Subject = 'Уведомление от СДС "РОССЕРТИФИКАЦИЯ"  о необходимости проведения второго инспекционного контроля';
    $mail->Body = preg_replace($patterns, array_values($c), $template2);
    $is_send2 = $mail->send();
  };

  if ($is_send || $is_send2) {
    /* ставим дату когда отправляли */
    $nowdt = date("Y-m-d H:i:s");
    $dbh->query("UPDATE company SET date_email = '$nowdt' WHERE id = " . $c['id']);
    if($is_send ) $i++;
    if($is_send2 ) $i2++;

  }
}
/* выодим статистику сколько отправили по первому и второму IK*/
echo  "IK 1 = $i<br> IK 2 = $i2";
