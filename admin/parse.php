<?php
/**
 * Created by PhpStorm.
 * User: suyar
 * Date: 28.05.2018
 * Time: 23:22
 */

include_once '../config.php';

ini_set('error_reporting', E_ERROR);

if (isset($_GET['str'])) {
    $res = array('error' => '');
    $str = trim($_GET['str']);
    $url = 'https://www.rusprofile.ru';
    // https://www.rusprofile.ru/search?query=1024201389772
    $d = file_get_contents($url . '/search?query=' . $str);
    //$d = file_get_contents('../innsource.txt');
    if (strlen($d) > 30000) {
        $data = array();
        preg_match('#<h1 itemprop="name">.*?</h1>#s', $d, $m);
        if (isset($m[0])) $data['short_name'] = trim(strip_tags(html_entity_decode($m[0])));

        preg_match('#<div class="col-sm-10" itemprop="legalName">.*?</div>#s', $d, $m);
        if (isset($m[0])) $data['full_name'] = trim(strip_tags(html_entity_decode($m[0])));


        preg_match('/data-inn="(\d*)"/', $d, $m);
        if (isset($m[1])) $data['inn'] = trim(strip_tags($m[1]));

        preg_match('/data-ogrn="(\d*)"/', $d, $m);
        if (isset($m[1])) $data['ogrn'] = trim(strip_tags($m[1]));


        preg_match('#<span title="Код причины постановки"><strong>.*?<div class="col-tri">.*?(\w+).*?</div>#s', $d, $m);
        if (isset($m[1])) $data['kpp'] = trim(strip_tags($m[1]));

        preg_match('#<span title="Общероссийский классификатор предприятий и организаций"><strong>.*?<div class="col-tri">.*?(\w+).*?</div>#s', $d, $m);
        if (isset($m[1])) $data['okpo'] = trim(strip_tags($m[1]));


        preg_match('#itemprop="address" .*?>(.*?)div#s', $d, $m);
        if (isset($m[1])) {
            $data['adress_doc'] = trim(strip_tags($m[1]));
            $data['adress_doc'] = str_replace("\n", " ", $data['adress_doc']);
            $data['adress_doc'] = str_replace("  ", "", $data['adress_doc']);
        }
        if (isset($data['adress_doc'])) $data['adress_fact'] = $data['adress_doc'];

        preg_match('#<div class="col-left dl" itemprop="jobTitle">(.*?)</div>#s', $d, $m);
        if (isset($m[1])) $data['work'] = trim(strip_tags($m[1]));

        preg_match('#jobTitle.*?<span itemprop="name">(.*?)<br>#s', $d, $m);
        if (isset($m[1])) $data['worker'] = trim(strip_tags($m[1]));

        if (isset($data['worker'])) $data['auditor_1'] = $data['worker'];

        /* Регистрационный номер (Генерируем автоматически через выражение "ROSSERT.RU.ДЕНЬМЕСЯЦ.ИНН", подставляем текущий день и месяц, например ROSSERT.RU.2805.785165484) */
        $data['register_number'] = "ROSSERT.RU." . date("dm") . ".$inn";
        /*Порядковый номер (Генерируем автоматически начиная с 230001)*/

        $data['id_main'] = 230001;
        $id_main = $dbh->query("SELECT MAX(id_main)+1 FROM company ")->fetchColumn(0);
        if ($id_main > $data['id_main']) $data['id_main'] = $id_main;

        /*Дата выдачи (Подставляем сегодняшнее число в виде "28 мая 2018г.")*/
        $res['data'] = $data;

    } else {
        $res['error'] = "Данный ОГРН не найден";
    }

    echo json_encode($res);
}