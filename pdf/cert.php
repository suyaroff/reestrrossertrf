<?php
/**
 * Created by PhpStorm.
 * User: suyar
 * Date: 03.06.2018
 * Time: 0:43
 */

include_once '../config.php';
require ROOT_PATH . '/vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;

$company_id = 0;
if (isset($_GET['company'])) $company_id = (int)$_GET['company'];

if ($company_id) {
  $comp = db_get_where('company', "id = $company_id");
  if (count($comp)) {
    $company = $comp[0];
    $company['now'] = date("d.m.Y");

    $company['date_open'] = date("d.m.Y", strtotime($company['date_open']));
    $company['date_close'] = date("d.m.Y", strtotime($company['date_close']));
    $company['date_control_1'] = date("d.m.Y", strtotime($company['date_control_1']));


    $patterns = array_map(function ($k) {
      return "/\{$k\}/";
    }, array_keys($company));

    $cert = 1;
    if(isset($_GET['cert'])) $cert = (int)$_GET['cert'];
    // Загружаем шаблон
    $html = file_get_contents(ROOT_PATH.'/template/pdf/cert/'.$cert.'.html');

    $html = preg_replace($patterns, array_values($company), $html);
    $html = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');


    $options['isJavascriptEnabled'] = false;
    $options['isRemoteEnabled'] = true;
    $options['isHtml5ParserEnabled'] = true;
    $options['fontDir'] = ROOT_PATH.'/pdf/fonts';

    $dompdf = new Dompdf($options);
    $dompdf->loadHtml($html, 'UTF-8');

    $dompdf->setPaper('A4');

    $dompdf->render();

    $file_name = "$company[date_open]_$company[inn].pdf";

    $dompdf->stream($file_name);
  } else {
    echo "Company not exists";
  }

}