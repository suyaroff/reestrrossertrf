<section class="all-section">
  <h1>Все записи</h1>
  <table>
    <thead>
    <tr>
      <th>Краткое наименование</th>
      <th>ИНН</th>
      <th>Дата выдачи</th>
      <th>Действителен до</th>
      <th>1-й инспекционный контроль</th>
      <th>2-й инспекционный контроль</th>
      <th>Статус сертификата</th>
      <th>Отказ от ИК</th>
      <th></th>
    </tr>
    </thead>
    <tbody>
    <? foreach ($companies as $c){ ?>
      <tr >
        <td>
          <a href="/admin/?action=edit_company&id=<? echo $c['id'];?>"><? echo $c['short_name'];?></a>
          <a href="/company/?id=<? echo $c['id'];?>" target="_blank"><small>посмотреть</small></a>
        </td>
        <td><? echo $c['inn'];?></td>
        <td><? echo $c['date_open'];?></td>
        <td><? echo $c['date_close'];?></td>
        <td><? echo $c['date_control_1'];?> / <? echo $c['status_date_1'];?></td>
        <td><? echo $c['date_control_2'];?> / <? echo $c['status_date_2'];?></td>
        <td><? if($c['status']) echo 'действителен'; else echo 'приостановлен';?></td>
        <td class="<? if($c['refused_ik'] == 1) echo 'refused_cell';?>"><? if($c['refused_ik'] == 1) echo 'да'; else echo 'нет';?></td>
        <td>
          <a href="/admin/?delete_company=<? echo $c['id'];?>">удалить</a>
        </td>
      </tr>
    <? } ?>
    </tbody>
  </table>
</section>